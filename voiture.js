class Voiture {
  constructor(nbRoue = 4, marque, modele,annee) {
    this.nbRoue = nbRoue; //propriété = paramètre
    this.marque = marque;
    this.modele=modele;
    this.annee=annee;
  }
  
}


class GlobalFunctions {

  static retrograde() {
    console.log("retrograde");
  }

  static upgrade() {
    console.log("upgrade");
  }

  static getCurrentTime() {
    return new Date();
  }
}
