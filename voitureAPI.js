class VoitureAPI {
  static getAll() {
    var voitures = [
      new Voiture(4, "renault", "clio", 2000),
      new Voiture(4, "renault", "clio 3", 2008),
    ];

    const nb=50;

    for (var i=0; i<nb; i++){
        voitures.push(new Voiture(4,"audi","quattro",2000+i));
    }
    
    return voitures;

  }


  //ex intégration API Js
  static async fetchAll() {
    //Obligé async en API pour exec arrière plan + racourci
    const url = "https://webinstart.com";

    //resp format json il faut encore le transformer
    const response = await fetch(url, {
      method: "GET", //par defaut method get s'applique
    });

    if (response.status >= 200 && response.status <= 300) {
      const data = await response.json();
      return data;
    }
  }
}
